package com.example.sbvm.dao;

import com.example.sbvm.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 * 【类名】: SysRoleMapper .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/27 11:47 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Mapper
public interface SysRoleMapper {
    UserEntity findOneByUsername(String userName);
}
