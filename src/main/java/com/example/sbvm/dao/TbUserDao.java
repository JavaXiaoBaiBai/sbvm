package com.example.sbvm.dao;

import com.example.sbvm.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <pre>
 * 【类名】: TbUserDao .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/22 10:42 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
public interface TbUserDao {
    User selectUserByMobileNo(String mobileNo);
    List<User> getUserByMobileNo(String mobileNo);
}
