package com.example.sbvm.entity;

import lombok.Data;

/**
 * <pre>
 * 【类名】: PermissionEntity .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/7/4 16:02 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
public class PermissionEntity {
    private String permissionId;
    private String premissionName;
}
