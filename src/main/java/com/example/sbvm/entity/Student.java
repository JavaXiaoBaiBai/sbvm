package com.example.sbvm.entity;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * 【类名】: Student .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/25 10:56 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
@Component
@ConfigurationProperties(prefix = "student")
public class Student {
    private String name;
    private String age;
    private String school;
    private String gender;
}
