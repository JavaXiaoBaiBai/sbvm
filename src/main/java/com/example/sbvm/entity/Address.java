package com.example.sbvm.entity;

import lombok.Data;

/**
 * <pre>
 * 【类名】: Address .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/7/4 10:25 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
public class Address {
    private String province;
    private String city;
    private String area;
}
