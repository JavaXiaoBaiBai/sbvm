package com.example.sbvm.entity;

import lombok.Data;

import java.util.List;

/**
 * <pre>
 * 【类名】: RoleEntity .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/7/4 16:02 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
public class RoleEntity {
    private String roleId;
    private String roleName;
    private List<PermissionEntity> permissionEntityList;
}
