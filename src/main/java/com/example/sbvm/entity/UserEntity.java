package com.example.sbvm.entity;

import lombok.Data;

import java.util.List;

/**
 * <pre>
 * 【类名】: UserEntity .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/27 10:00 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
public class UserEntity {
    private String id;
    private String userName;
    private String passWord;
    private String phone;
    private String age;
    private String sex;
    private int status;
    /**
     * 一个用户具有多个角色
     */
    private List<RoleEntity> roleList;

    public String getCredentialsSalt(){
        return userName + passWord;
    }
}
