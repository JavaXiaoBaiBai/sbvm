package com.example.sbvm.entity;

import lombok.Data;

import java.util.List;

/**
 * <pre>
 * 【类名】: SysPermisEntity .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/27 10:37 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
public class SysPermisEntity {
    private String permisName;
    private List<RoleEntity> roles;


}
