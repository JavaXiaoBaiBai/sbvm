package com.example.sbvm.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * <pre>
 * 【类名】: SysRoleEntity .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/27 10:31 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
public class SysRoleEntity {
    private String roleName;
    /**
     * 一个角色对应多个权限
     */
    private List<PermissionEntity> permisList;
    /**
     * 一个角色对应多个用户
     */
    private List<UserEntity> userList;



    public List<String> getPermissionsName() {
        List<String> list = new ArrayList<String>();
        List<PermissionEntity> preList = getPermisList();
        for (PermissionEntity per : preList) {
            list.add(per.getPremissionName());
        }
        return list;

    }
}
