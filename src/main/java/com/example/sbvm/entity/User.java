package com.example.sbvm.entity;

import lombok.Data;

/**
 * <pre>
 * 【类名】: User .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/22 10:43 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
public class User {
    private String name;
    private String mobileNo;
    private String customerId;


}
