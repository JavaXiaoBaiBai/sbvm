package com.example.sbvm.config;


import lombok.Data;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 【类名】: ShiroConfig .</br>
 * 【作用】: Shiro 配置.</br>
 * 【时间】：2018/6/27 10:55 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "spring.shiroFilter")
public class ShiroConfig {

    private static final Logger logger = LoggerFactory
            .getLogger(ShiroConfig.class);
    private List<String> shiroFilter;

    private String loginUrl;

    private String successUrl;

    private String unauthorizedUrl;

    @Bean
    public ShiroFilterFactoryBean shiroFilter(org.apache.shiro.mgt.SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        //获取filters
        Map<String, Filter> filters = shiroFilterFactoryBean.getFilters();
        //将自定义 的ShiroFilterFactoryBean注入shiroFilter
        filters.put("perms", new ShiroPermissionsFilter());

        // 必须设置SecuritManager
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        // 这里自定义的权限拦截规则
        // filterChainDefinitionMap.put("/system/*/add", "perms[add]");
        // filterChainDefinitionMap.put("/system/*/delete", "perms[del]");
        // filterChainDefinitionMap.put("/system/*/list", "perms[list]");
        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面，这个就是类似于登录界面
        shiroFilterFactoryBean.setLoginUrl(loginUrl);
        // 登录成功后要跳转的链接
        shiroFilterFactoryBean.setSuccessUrl(successUrl);

        // 未授权界面;
        shiroFilterFactoryBean.setUnauthorizedUrl(unauthorizedUrl);
        // 拦截器.
        shiroFilterFactoryBean
                .setFilterChainDefinitionMap(getFileChainDefinitionMap());

        logger.info("--------------Shiro拦截器工厂类注入成功----------------");
        return shiroFilterFactoryBean;
    }

    /**
     * 【方法名】: authRealm.配置自定义的权限登录器<br/>
     * 【作者】: @author 张翔宇 .<br/>
     * 【时间】： 2018-06-178  10:57 .<br/>;
     * 【参数】： .<br/>
     * @param  "".<br/>
     * @return  <p>
     * 修改记录.<br/>
     * 修改人: 张翔宇 修改描述：创建新新件 .<br/>
     * .<p/>
     */
    @Bean
    public AuthRealm authRealm() {
        AuthRealm authRealm = new AuthRealm();
        //  authRealm.setCredentialsMatcher(matcher);
        return authRealm;
    }

    //配置核心安全事务管理器
    @Bean
    public org.apache.shiro.mgt.SecurityManager securityManager() {
        logger.info("--------------shiro安全事务管理器已经加载----------------");
        DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
        manager.setRealm(authRealm());
        return manager;
    }

    public Map<String, String> getFileChainDefinitionMap(){
        Map<String, String> fileChanDefinitionMap = new LinkedHashMap<String, String>();
        for (String str : shiroFilter) {
            String[] split = str.split("=");
            if (split.length < 2) {
                logger.error(split[0] + "配置出现异常！");
            }
            for (int i = 0; i< split.length; i++) {
                fileChanDefinitionMap.put(split[0], split[1]);
            }
        }
        return fileChanDefinitionMap;
    }
}
