package com.example.sbvm.config;

import com.example.sbvm.entity.*;
import com.example.sbvm.service.UserService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <pre>
 * 【类名】: AuthRealm .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/27 10:59 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
public class AuthRealm extends AuthorizingRealm {
    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory
            .getLogger(AuthRealm.class);

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principals) {
        // TODO Auto-generated method stub
        logger.info("--------------权限配置——授权----------------");
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        UserEntity user = (UserEntity) principals.getPrimaryPrincipal();
        for (RoleEntity role : user.getRoleList()) {
            authorizationInfo.addRole(role.getRoleName());
            for (PermissionEntity p : role.getPermissionEntityList()) {
                authorizationInfo.addStringPermission(p.getPremissionName());
            }
        }
        logger.info(user.toString());
        return authorizationInfo;
    }

    /**
     * 认证信息.(身份验证) : Authentication 是用来验证用户身份 如果返回一个SimpleAccount
     * 对象则认证通过，如果返回值为空或者异常，则认证不通过。 1、检查提交的进行认证的令牌信息 2、根据令牌信息从数据源(通常为数据库)中获取用户信息
     * 3、对用户信息进行匹配验证 4、验证通过将返回一个封装了用户信息的AuthenticationInfo实例
     * 5、验证失败则抛出AuthenticationException异常信息
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken token) throws AuthenticationException {
        // TODO Auto-generated method stub
        logger.info("***用户身份验证");
        // 获取用户的输入的账号.
        String username = (String) token.getPrincipal();
        if ("".equals(username) || username == null) {
            return null;
        }
        logger.info("***" + token.getCredentials());
        // 实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        UserEntity user = userService.selectOne(username);

        logger.info("***登录user：" + user);
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                // 用户名
                user,
                // 密码
                user.getPassWord(),
                // 这里的getCredentialsSalt()只是返回一个唯一值，我返回的是用户名，用来加密的 salt=username+salt
                ByteSource.Util.bytes(user.getCredentialsSalt()),
                // realm name
                getName()
        );
        return authenticationInfo;
    }
}
