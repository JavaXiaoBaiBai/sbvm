package com.example.sbvm.controller;


import com.example.sbvm.entity.Address;
import com.example.sbvm.entity.Student;
import com.example.sbvm.entity.User;
import com.example.sbvm.service.FirstService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 * 【类名】: FirstController .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/22 10:07 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Controller
@RequestMapping(value = "/first")
public class FirstController {
    @Resource
    FirstService firstService;

    @Resource
    Student student;

    /**
     * 【方法名】: sayHello.<br/>
     * 【作者】: @author 张翔宇 .<br/>
     * 【时间】： 2018-06-176  14:02 .<br/>;
     * 【参数】： .<br/>
     * @param req .<br/>
     * @param resp .<br/>
     * @return String <p>
     * 修改记录.<br/>
     * 修改人: 张翔宇 修改描述：创建新新件 .<br/>
     * .<p/>
     */
    @RequestMapping(value = "/login")
    public String sayHello(HttpServletRequest req, HttpServletResponse resp){
        return "login";
    }


    /**
     * 【方法名】: getUser.<br/>
     * 【作者】: @author 张翔宇 .<br/>
     * 【时间】： 2018-06-176  14:03 .<br/>;
     * 【参数】： .<br/>
     * @param mobileNo .<br/>
     * @return String <p>
     * 修改记录.<br/>
     * 修改人: 张翔宇 修改描述：创建新新件 .<br/>
     * .<p/>
     */
    @RequestMapping(value = "/getUser")
    @ResponseBody
    public String getUser(String mobileNo){
        if (mobileNo == null || "".equals(mobileNo)) {
            return "请输入手机号";
        }
        return firstService.getUserList(mobileNo).toString();
    }

    /**
     * 【方法名】: firstTy.<br/>
     * 【作者】: @author 张翔宇 .<br/>
     * 【时间】： 2018-06-176  14:03 .<br/>;
     * 【参数】： .<br/>
     * @param model .<br/>
     * @param name .<br/>
     * @return String <p>
     * 修改记录.<br/>
     * 修改人: 张翔宇 修改描述：创建新新件 .<br/>
     * .<p/>
     */
    @RequestMapping(value = "/ty")
    public String firstTy(Model model, String name, HttpServletRequest req) {
        if ("".equals(name) || null == name) {
            name = student.getName();
        }
        model.addAttribute("name",name);
        model.addAttribute("student",student);
        req.getSession().setAttribute("student", student);

        // List<User> list = firstService.getUserList("13546235304");
        // model.addAttribute("user",list);

        return "first";
    }
    /** 
     * 【方法名】: toCaptchaPage.<br/>
     * 【作者】: @author 张翔宇 .<br/>
     * 【时间】： 2018-07-183  14:05 .<br/>;
     * 【参数】： .<br/>
     * @param  request .<br/>
     * @return  <p>
     * 修改记录.<br/>
     * 修改人: 张翔宇 修改描述：创建新新件 .<br/>
     * .<p/>
     */
    @RequestMapping(value = "getCaptchaPage")
    public String getCaptchaPage(HttpServletRequest request, Model model){
        model.addAttribute("check",true);
        return "captcha";
    }
    /** 
     * 【方法名】: getChinaMap.<br/>
     * 【作者】: @author 张翔宇 .<br/>
     * 【时间】： 2018-07-184  17:12 .<br/>;
     * 【参数】： .<br/>
     * @param  null .<br/>
     * @return  <p>
     * 修改记录.<br/>
     * 修改人: 张翔宇 修改描述：创建新新件 .<br/>
     * .<p/>
     */
    @RequestMapping(value = "getChinaMap")
    public String getChinaMap() {
        return "chinaMap";
    }

    @RequestMapping(value = "getArea")
    @ResponseBody
    public Map<String, String> getArea(Address address, HttpServletRequest req) {
        Map<String, String> resultMap = new HashMap<String, String>();
        req.getSession().setAttribute("areaArray", address);
        resultMap.put("msg", "success");
        return resultMap;
    }

    public boolean EmptyChecker(String str){
        if (str == null || "".equals(str)) {
            return false;
        }
        return true;
    }
}
