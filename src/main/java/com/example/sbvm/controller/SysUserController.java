package com.example.sbvm.controller;

import com.example.sbvm.entity.UserEntity;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.thymeleaf.util.DateUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * 【类名】: SysUserController .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/27 14:21 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@RestController
@RequestMapping("login")
public class SysUserController {

    private static final Logger logger = LoggerFactory.getLogger(SysUserController.class);

    @RequestMapping("/validate")
    public Map<String, Object> validate(String userName, String passWord) {
        Map<String, Object> map = new HashMap<String,Object>();
        // 将密码进行md5加密
        // passWord = MD5Util.GetMD5Code(passWord);
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(userName, passWord);
        boolean flag = false;
        String msg = "";
        Subject subject = SecurityUtils.getSubject();
        try {
            subject.login(usernamePasswordToken);
            UserEntity user = (UserEntity) subject.getPrincipal();
            subject.getSession().setAttribute("LOGIN_USER", user);
            msg = "登录成功！";
            flag = true;
        } catch (Exception e) {
            if (e instanceof UnknownAccountException) {
                logger.info("账号不存在： -- > UnknownAccountException");
                msg = "登录失败，用户账号不存在！";
            } else if (e instanceof IncorrectCredentialsException) {
                logger.info(" 密码不正确： -- >IncorrectCredentialsException");
                msg = "登录失败，用户密码不正确！";
            } else {
                logger.info("else -- >" + e);
                msg = "登录失败，发生未知错误：" + e;
            }
        } finally {
            map.put("flag", flag);
            map.put("msg", msg);
            return map;
        }

    }
}
