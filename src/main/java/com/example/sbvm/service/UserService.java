package com.example.sbvm.service;

import com.example.sbvm.entity.UserEntity;

import java.util.List;

/**
 * <pre>
 * 【类名】: UserService .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/27 11:01 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
public interface UserService {
    UserEntity selectOne(String userName);
}
