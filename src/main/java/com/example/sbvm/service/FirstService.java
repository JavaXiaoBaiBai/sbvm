package com.example.sbvm.service;

import com.example.sbvm.entity.User;

import java.util.List;

/**
 * <pre>
 * 【类名】: FirstService .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/22 10:35 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
public interface FirstService {
    List<User> getUserList(String mobileNo);
    User getUser(String mobileNo);
}
