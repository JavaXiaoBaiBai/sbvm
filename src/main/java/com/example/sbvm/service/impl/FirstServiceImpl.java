package com.example.sbvm.service.impl;

import com.example.sbvm.dao.TbUserDao;
import com.example.sbvm.entity.User;
import com.example.sbvm.service.FirstService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <pre>
 * 【类名】: FirstServiceImpl .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/22 10:35 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Service
public class FirstServiceImpl implements FirstService{
    @Resource
    TbUserDao tbUserDao;

    @Override
    public User getUser(String mobileNo) {
        return tbUserDao.selectUserByMobileNo(mobileNo);
    }

    @Override
    public List<User> getUserList(String mobileNo) {
        return tbUserDao.getUserByMobileNo(mobileNo);
    }
}
