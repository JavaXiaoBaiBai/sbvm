package com.example.sbvm.service.impl;

import com.example.sbvm.dao.SysRoleMapper;
import com.example.sbvm.entity.UserEntity;
import com.example.sbvm.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <pre>
 * 【类名】: UserServiceImpl .</br>
 * 【作用】: ${TODO} ADD FUNCTION. &.</br>
 * 【时间】：2018/6/27 11:01 .</br>
 * 【作者】：@author xinxiang .</br>
 * </pre>
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    SysRoleMapper sysRoleMapper;

    @Override
    public UserEntity selectOne(String userName) {
        return sysRoleMapper.findOneByUsername(userName);
    }
}
